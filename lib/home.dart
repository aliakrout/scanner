import 'package:flutter/material.dart';
import 'package:scanner/scanner.dart';


class Home extends StatefulWidget {
  const Home({super.key});

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    return  Scaffold(
      body:  Center(
        child: GestureDetector(
          onTap: (){
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => Scanner()),
            );
          },
          child: Container(
          width: 200,
          height: 50,
          color: Colors.black,
          child: const Center(
            child: Text("Scan",style: TextStyle(color: Colors.white),),
          ),
          ),
        ),
      ),
    );
  }
}